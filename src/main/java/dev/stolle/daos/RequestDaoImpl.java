package dev.stolle.daos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import dev.stolle.models.Request;
import dev.stolle.models.User;
import dev.stolle.util.ConnectionUtil;

public class RequestDaoImpl implements RequestDao {
	private static Logger log = Logger.getRootLogger();
	private UserDao userDao = new UserDaoImpl();
	
	public boolean approveRequest(Request  request) throws SQLException {
		log.info("Approving request: " + request.getRequestId());
		String sql = "UPDATE Requests SET Status = ? WHERE Request_Id = ?";
		Connection conn = null;
		boolean approved = false;
		try {
			conn = ConnectionUtil.getConnection();
			try(PreparedStatement ps = conn.prepareStatement(sql);){
				ps.setString(1, "APPROVED");
				ps.setInt(2, request.getRequestId()); 
				int update = ps.executeUpdate();
				if (update == 1) {
					log.info("Successfully approved request: " + request.getRequestId());
					if(userDao.updateUserReimbursement(userDao.getUser(request.getUserId()), request.getAmount())) {
						approved = true;
					}
				} 
			}
		} catch (SQLException ex) {
			log.error("Error approving request: " + request.getRequestId() + ". SQLException caught.");
			ex.printStackTrace();
		} finally {
			if(conn != null) {
				conn.close();
			}
		}
		
		return approved;
	}
	
	public boolean rejectRequest(Request request) throws SQLException {
		log.info("Rejecting request: " + request.getRequestId());
		String sql = "UPDATE Requests SET Status = ? WHERE Request_Id = ?";
		Connection conn = null;
		boolean rejected = false;
		try {
			conn = ConnectionUtil.getConnection();
			try(PreparedStatement ps = conn.prepareStatement(sql);){
				ps.setString(1, "REJECTED");
				ps.setInt(2, request.getRequestId()); 
				int update = ps.executeUpdate();
				if (update == 1) {
					log.info("Successfully rejected request: " + request.getRequestId());
					rejected = true;
				} 
			}
		} catch (SQLException ex) {
			log.error("Error rejecting request: " + request.getRequestId() + ". SQLException caught.");
			ex.printStackTrace();
		} finally {
			if(conn != null) {
				conn.close();
			}
		}
		
		return rejected;
	}
	
	public boolean submitRequest(Request request) throws SQLException {
		log.info("Submitting request");
		String sql = "INSERT INTO Requests (User_Id,Amount) VALUES (?,?)";
		boolean submitted = false;
		Connection conn = null;
		try {
			conn = ConnectionUtil.getConnection();
			try(PreparedStatement ps = conn.prepareStatement(sql);){
				conn.setAutoCommit(false);
				ps.setInt(1, request.getUserId());
				ps.setDouble(2, request.getAmount());
				ps.execute();
				submitted = true;
				conn.commit();
			}
		} catch (SQLException ex) {
			if(conn != null) {
				conn.rollback();
			} 
			ex.printStackTrace();
		} finally {
			if(conn != null) {
				conn.close();
			}
		}
		
		return submitted;
	} 
}
