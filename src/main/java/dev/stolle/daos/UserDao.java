package dev.stolle.daos;

import java.sql.SQLException;
import java.util.List;

import dev.stolle.models.Request;
import dev.stolle.models.User;

public interface UserDao {
	public User getUser(String userName, String password);
	public User getUser(int userId);
	public List<Request> getAllUserRequests(User user);
	public List<Request> getPendingRequests(User user);
	public List<Request> getResolvedRequests(User user);
	public List<Request> getAllPendingRequests();
	public boolean updateUserReimbursement(User user, double amount) throws SQLException;
}
