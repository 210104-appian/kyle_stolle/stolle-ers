package dev.stolle.services;

import java.sql.SQLException;
import java.util.List;

import dev.stolle.daos.RequestDao;
import dev.stolle.daos.RequestDaoImpl;
import dev.stolle.daos.UserDao;
import dev.stolle.daos.UserDaoImpl;
import dev.stolle.models.Request;
import dev.stolle.models.User;

public class ManagerService {
	
	private UserDao managerUserDao = new UserDaoImpl();
	private RequestDao managerRequestDao = new RequestDaoImpl();
	
	public User getUser(int userId) {
		return managerUserDao.getUser(userId);
	}
	
	public List<Request> getAllPendingRequests(){
		return managerUserDao.getAllPendingRequests();
	}
	
	public boolean approveRequest(Request request) {
		boolean success = false; 
		try{
			success = managerRequestDao.approveRequest(request);
		} catch(SQLException ex) {
			System.out.println("Failed approving request.");
			ex.printStackTrace();
		}
		
		return success;
	}
	
	public boolean rejectRequest(Request request) {
		boolean success = false; 
		try{
			success = managerRequestDao.rejectRequest(request);
		} catch(SQLException ex) {
			System.out.println("Failed rejecting request.");
			ex.printStackTrace();
		}
		
		return success;
	}


}
