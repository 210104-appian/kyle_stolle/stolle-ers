package dev.stolle.models;

import java.util.List;

public class User {
	private int userId;
	private String userName;
	private String password;
	private String firstName;
	private String lastName;
	private String birthday;
	private double totalReimbursementAmount;
	private UserType userType; 
	private List<Request> requests;
	
	public User() {
		super();
	}
	
	public User(int userId, String userName, String password, String firstName, String lastName, String birthday, double totalReimbursementAmount, UserType userType, List<Request> requests) {
		super();
		setUserId(userId);
		setUserName(userName);
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setBirthday(birthday);
		setTotalReimbursementAmount(totalReimbursementAmount);
		setUserType(userType);
		setRequests(requests);
	}
	
	public User(int userId, String userName, String password, String firstName, String lastName, String birthday, double totalReimbursementAmount, UserType userType) {
		super();
		setUserId(userId);
		setUserName(userName);
		setPassword(password);
		setFirstName(firstName);
		setLastName(lastName);
		setBirthday(birthday);
		setTotalReimbursementAmount(totalReimbursementAmount);
		setUserType(userType);
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getBirthday() {
		return birthday;
	}

	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}

	public double getTotalReimbursementAmount() {
		return totalReimbursementAmount;
	}

	public void setTotalReimbursementAmount(double totalReimbursementAmount) {
		this.totalReimbursementAmount = totalReimbursementAmount;
	}

	public UserType getUserType() {
		return userType;
	}

	public void setUserType(UserType userType) {
		this.userType = userType;
	}

	public List<Request> getRequests() {
		return requests;
	}

	public void setRequests(List<Request> requests) {
		this.requests = requests;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((birthday == null) ? 0 : birthday.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((requests == null) ? 0 : requests.hashCode());
		long temp;
		temp = Double.doubleToLongBits(totalReimbursementAmount);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + userId;
		result = prime * result + ((userName == null) ? 0 : userName.hashCode());
		result = prime * result + ((userType == null) ? 0 : userType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (birthday == null) {
			if (other.birthday != null)
				return false;
		} else if (!birthday.equals(other.birthday))
			return false;
		if (firstName == null) {
			if (other.firstName != null)
				return false;
		} else if (!firstName.equals(other.firstName))
			return false;
		if (lastName == null) {
			if (other.lastName != null)
				return false;
		} else if (!lastName.equals(other.lastName))
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (requests == null) {
			if (other.requests != null)
				return false;
		} else if (!requests.equals(other.requests))
			return false;
		if (Double.doubleToLongBits(totalReimbursementAmount) != Double
				.doubleToLongBits(other.totalReimbursementAmount))
			return false;
		if (userId != other.userId)
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		if (userType != other.userType)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", password=" + password + ", firstName="
				+ firstName + ", lastName=" + lastName + ", birthday=" + birthday + ", totalReimbursementAmount="
				+ totalReimbursementAmount + ", userType=" + userType + ", requests=" + requests + "]";
	}

	
	
	
	
	
}
