const baseUrl = "http://localhost:8080/stolle-esr";
const loadEmployeeUrl = "/employee?userId=";
const submitUrl = "/employee"
// event listeners
$(window).on("load", function(){
    // retrieve user from server 
    performAjaxGetRequest((baseUrl + loadEmployeeUrl + sessionStorage.userId));
    
    // add gui to pages based on what was returned
});

document.getElementById("btnOpenNewRequest").addEventListener("click", function(){
    // open new request modal
    $("#newRequestModal").modal("show");
});

document.getElementById("btnSubmitNewRequest").addEventListener("click",function (){
    // turn data into json 
    // send post request
    let amount = parseFloat(document.getElementById("requestedAmountTxt").value);
    let request = {
        "userId": sessionStorage.userId,
        "amount": amount
    }
    let jsonRequest = JSON.stringify(request);
    $("#newRequestModal").modal("hide");
    performAjaxPostRequest((baseUrl+submitUrl),jsonRequest);
});


function performAjaxPostRequest(url,data){
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function() {
            console.log("Successfully submitted new request");
            window.location.reload();
        }, 
    });
}

function performAjaxGetRequest(url){
    $.get(url, function (data,textStatus, jqXHR){
        console.log(textStatus);
        console.log(JSON.parse(data));
        updatePage(JSON.parse(data));
    });
}

function updatePage(user){
    // update personal information
    updatePersonalInformation(user);
    let pending = [];
    let resolved = [];
    for (let i = 0; i < user.requests.length;i++){
        if(user.requests[i].status === 'PENDING'){
            pending.push(user.requests[i]);
        } else {
            resolved.push(user.requests[i]);
        }
    }
    updatePendingRequests(pending);
    updateResolvedRequests(resolved);
}

function updateResolvedRequests(requests){
    for (let request of requests){
        let row = document.createElement("tr");
        let amount = document.createElement("td");
        let status = document.createElement("td");

        amount.innerHTML = "$" + request.amount;
        status.innerHTML = request.status;
        row.append(amount);
        row.append(status); 
        document.getElementById("resolvedTable").append(row);
    }
}

function updatePendingRequests(requests){
    for (let request of requests){
        let row = document.createElement("tr");
        let amount = document.createElement("td");
        let status = document.createElement("td");

        amount.innerHTML = "$" + request.amount;
        status.innerHTML = request.status;
        row.append(amount);
        row.append(status); 
        document.getElementById("pendingTable").append(row);
    }
}

function updatePersonalInformation(user){
    document.getElementById("userNameTxt").value = user.userName;
    document.getElementById("firstNameTxt").value = user.firstName;
    document.getElementById("lastNameTxt").value = user.lastName;
    let bday = user.birthday;
    let year = bday.substring(0,4);
    let month = bday.substring(5,7);
    let day = bday.substring(8,10);
    let birthday = month+"-"+day+"-"+year;
    document.getElementById("birthdayTxt").value = birthday;
    document.getElementById("totalReimbursementTxt").value = user.totalReimbursementAmount;
}
