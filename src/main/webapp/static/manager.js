const baseUrl = "http://localhost:8080/stolle-esr";
const loadManagerUrl = "/manager?userId=";
const managerUrl = "/manager";

// event listeners
$(window).on("load", function(){
    // retrieve user from server 
    performAjaxGetRequest((baseUrl + loadManagerUrl + sessionStorage.userId));
    
    // add gui to pages based on what was returned
});

document.getElementById("btnOpenApproveRequest").addEventListener("click", function(){
    $("#approveRequestModal").modal("show");
});

document.getElementById("btnOpenRejectRequest").addEventListener("click", function() {
    $("#rejectRequestModal").modal("show");
});

document.getElementById("btnRejectRequest").addEventListener("click", function(){
    let requestId = parseInt(document.getElementById("rejectRequestIdTxt").value);
    let userId = parseInt(document.getElementById("userId"+requestId).innerHTML);
    let amount = document.getElementById("amount"+requestId).innerHTML;
    let parsedAmount = parseFloat(amount.substring(1,amount.length));
    
    let request = {
        "requestId": requestId,
        "userId": userId,
        "amount": parsedAmount,
        "status": "REJECTED"
    }

    let requestJson = JSON.stringify(request);
    $("#rejectRequestModal").modal("hide");
    performAjaxPostRequest((baseUrl+managerUrl),requestJson);
});

document.getElementById("btnApproveRequest").addEventListener("click", function() {
    let requestId = parseInt(document.getElementById("requestedIdTxt").value);
    let userId = parseInt(document.getElementById("userId"+requestId).innerHTML);
    let amount = document.getElementById("amount"+requestId).innerHTML;
    let parsedAmount = parseFloat(amount.substring(1,amount.length));
    
    let request = {
        "requestId": requestId,
        "userId": userId,
        "amount": parsedAmount,
        "status": "APPROVED"
    }

    let requestJson = JSON.stringify(request);
    $("#approveRequestModal").modal("hide");
    performAjaxPostRequest((baseUrl+managerUrl),requestJson);

});

function performAjaxPostRequest(url,data){
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function() {
            console.log("Successfully approved request");
            window.location.reload();
        }, 
    });
}

function performAjaxGetRequest(url){
    $.get(url, function (data,textStatus, jqXHR){
        console.log(textStatus);
        console.log(data);
        updatePage(JSON.parse(data));
    });
}

function updatePage(user){
    updatePersonalInformation(user);
    updateRequests(user.userRequests);

}

function updateRequests(requests){
    for (let request of requests){
        let row = document.createElement("tr");
        let requestId = document.createElement("td");
        let userId = document.createElement("td");
        let amount = document.createElement("td");

        requestId.innerHTML = request.requestId;
        requestId.setAttribute("id","requestId"+request.requestId);
        userId.innerHTML = request.userId;
        userId.setAttribute("id","userId"+request.requestId);
        amount.innerHTML = "$" + request.amount;
        amount.setAttribute("id", "amount"+request.requestId);
        
        row.append(requestId);
        row.append(userId);
        row.append(amount);
        document.getElementById("requestsTableBody").append(row);
    }
}

function updatePersonalInformation(user){
    document.getElementById("userNameTxt").value = user.userName;
    document.getElementById("firstNameTxt").value = user.firstName;
    document.getElementById("lastNameTxt").value = user.lastName;
    let bday = user.birthday;
    let year = bday.substring(0,4);
    let month = bday.substring(5,7);
    let day = bday.substring(8,10);
    let birthday = month+"-"+day+"-"+year;
    document.getElementById("birthdayTxt").value = birthday;
}