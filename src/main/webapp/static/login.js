const baseUrl = "http://localhost:8080/stolle-esr";
const loginUrl = "/login";

// event listener 
document.getElementById("btnLogin").addEventListener("click", function (){
    let userName = document.getElementById("userNameInput").value;
    let password = document.getElementById("passwordInput").value;
    // construct json object 
    // send json object through post request to login url
    // parse json returned 
    // open new page based on parsed data
    let user = {
        "userName": userName,
        "password": password
    }
    let jsonUser = JSON.stringify(user);
    performAjaxPostRequest((baseUrl+loginUrl),jsonUser);
    

});

function performAjaxPostRequest(url, data){
    $.ajax({
        type: 'POST',
        url: url,
        data: data,
        success: function(data) {
            // open either manager or employee or send error based on data
            let userType = data.userType;
            sessionStorage.setItem("userId", data.userId);
            if (userType === 'EMPLOYEE'){
                window.location = "employee.html";
            } else if (userType === 'MANAGER') {
                window.location = "manager.html";
            } else {
                alert("Error. Something is wrong. Data was retrieved but userType was not accessible.");
            }
        }, 
        dataType: 'json'
    });
}